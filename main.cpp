#include <stdio.h>

int main()
{
       int linha,coluna;
       int matriz[3][3];
       int MENOR=0;

       for(linha=0; linha<=2; linha++){
           for(coluna=0; coluna<=2; coluna++)
{
               printf("\nDigite na linha 1 [%d] e coluna [%d]: ", linha+1, coluna+1);
               scanf("%d", &matriz[linha][coluna]);
           }

       }

       printf("\n Matriz Gerada\n\n");

       for(linha=0; linha<=2; linha++)
{
           for(coluna=0; coluna<=2; coluna++){
               printf("%d\t ", matriz[linha][coluna]);
           }
           printf("\n\n");
       }

   	MENOR = matriz[0][0];

       for(linha=0; linha<=2; linha++){
           for(coluna=0; coluna<=2; coluna++){

               if(matriz[linha][coluna] < MENOR)
{
                   MENOR = matriz[linha][coluna];

               }
           }
       }

       printf("\n MENOR: [%d]", MENOR);

       printf("\n\n");
       return 0;
}
